package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"golang.org/x/net/websocket"
)
//===========================
//Criando uma slice com make
//===========================
//Slices podem ser criadas com a makefunction interna; 
//É assim que você cria matrizes de tamanho dinâmico.
//A makefunção aloca uma matriz zerada e retorna uma fatia que se refere a essa matriz:



//===========================
//Flag
//===========================
//O sinalizador de pacote implementa a análise de sinalizador de linha de comando.



//===========================
//Channels
//===========================
//Os canais são um canal tipado através do qual você pode enviar e receber valores com o operador do canal <-
//ch <- v      	 Envia v para canalizar ch.
//v: = <-ch 	 Receber de ch e atribui valor ao v.
// Os dados fluem na direção da seta
// Canais . Se goroutines são as atividades de um programa Go simultâneo, os canais são as conexões entre eles.
// Um canal é um mecanismo de comunicação que permite que uma goroutine envie valores para outra goroutine .



//============================
//Map
//============================
//Estrutura de dados

type Message struct {
	
	Text 	 string `json:"text"`
}

type hub struct {
	clients          map[string]*websocket.Conn
	addClientChan    chan *websocket.Conn
	removeClientChan chan *websocket.Conn
	broadcastChan    chan Message
}

var (
	port = flag.String("port", "9000", "port used for ws connection")
)

func main() {
	flag.Parse()
	log.Fatal(server(*port))
}

// server cria um servidor websocket na porta <port> e registra o único handler
//=================================================================================
// server creates a websocket server at port <port> and registers the sole handler
func server(port string) error {
	h := newHub()
	mux := http.NewServeMux()
	mux.Handle("/", websocket.Handler(func(ws *websocket.Conn) {
		handler(ws, h)
	}))

	s := http.Server{Addr: ":" + port, Handler: mux}
	return s.ListenAndServe()
}
// handler registra um novo cliente de chat conn;
// Executa o hub, adiciona o cliente ao pool de conexão
// e transmite a mensagem recebida
//=======================================================
// handler registers a new chat client conn;
// It runs the hub, adds the client to the connection pool
// and broadcasts received message
func handler(ws *websocket.Conn, h *hub) {

//==========================================
//GOROUTINES
//==========================================
//Em Go, cada atividade em execução simultânea é chamada de goroutine . 
//Se você usou encadeamentos ou encadeamentos de sistemas operacionais em outros idiomas, então você pode assumir por enquanto que uma goroutine é semelhante a um encadeamento.
// As diferenças entre linhas e goroutines são essencialmente quantitativas, não qualitativas.
//Quando um programa começa, sua única goroutine é aquela que chama a mainfunção, então chamamos de a principal goroutine . Novas goroutines são criadas pela godeclaração:

//Sintaticamente, uma go instrução é uma função comum ou chamada de método prefixada pela palavra-chave go.
//Uma godeclaração faz com que a função seja chamada em uma nova goroutine. A instrução go completa-se imediatamente.
	go h.run()

	h.addClientChan <- ws

	for {
		var m Message
		err := websocket.JSON.Receive(ws, &m)
		if err != nil {
			h.broadcastChan <- Message{err.Error()}
			h.removeClient(ws)
			return
		}
		h.broadcastChan <- m
	}
}
// newHub retorna um novo objeto de hub
//==========================================
// newHub returns a new hub object
func newHub() *hub {
	return &hub{
		clients:          make(map[string]*websocket.Conn),
		addClientChan:    make(chan *websocket.Conn),
		removeClientChan: make(chan *websocket.Conn),
		broadcastChan:    make(chan Message),
	}
}
// o run recebe um retorno dos canais do hub e chama o método de hub apropriado
//===========================================================================
// run receives from the hub channels and calls the appropriate hub method
func (h *hub) run() {
	for {
		select {
		case conn := <-h.addClientChan:
			h.addClient(conn)
		case conn := <-h.removeClientChan:
			h.removeClient(conn)
		case m := <-h.broadcastChan:
			h.broadcastMessage(m)
		}
	}
}
// removeClient remove um conn de cliente do pool
//=======================================================
// removeClient removes a conn from the pool
func (h *hub) removeClient(conn *websocket.Conn) {
	delete(h.clients, conn.LocalAddr().String())
}
// addClient adiciona um conn ao pool
//=======================================================
// addClient adds a conn to the pool
func (h *hub) addClient(conn *websocket.Conn) {
	h.clients[conn.RemoteAddr().String()] = conn
}
// broadcastMessage envia uma mensagem para todos os clientes conns no pool
//===================================================================
// broadcastMessage sends a message to all client conns in the pool
func (h *hub) broadcastMessage(m Message) {
	for _, conn := range h.clients {
		err := websocket.JSON.Send(conn, m)
		if err != nil {
			fmt.Println("Error broadcasting message: ", err)
			return
		}
	}
}
