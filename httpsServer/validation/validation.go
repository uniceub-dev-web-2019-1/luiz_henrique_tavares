package validation

import ( 
	val "gopkg.in/go-playground/validator.v9"
		"luiz_henrique_tavares/httpsServer/database"
		"luiz_henrique_tavares/httpsServer/configs" )

// create a global validator
var Validator *val.Validate


// function to instanceate a validator
func CreateValidator() {

	// call create validator
	Validator = val.New()

	// create custom validation 
	Validator.RegisterValidation(configs.COLLECTION_USERS, ValidateUsingUser)
}

// validates if user's name is already in use 
func ValidateUsingUser (field val.FieldLevel) bool {
	userusing := field.Field().String()
	return database.FindUnavailableName(userusing, configs.COLLECTION_USERS) 
}