package server

import (
	"github.com/gorilla/mux"
	"net/http"
	"luiz_henrique_tavares/httpsServer/configs"
	"luiz_henrique_tavares/httpsServer/control"
)

func createHandler() (handler *mux.Router) {

	//creats router
	handler = mux.NewRouter()

	//========================================================
	// user handlers
	//========================================================
	handler.HandleFunc(configs.REGISTER_USER_PATH, control.RegisterUser).Methods(http.MethodPost)
	handler.HandleFunc(configs.LOGIN_USER_PATH,    control.LoginUser   ).Methods(http.MethodPost)
	handler.HandleFunc(configs.UPDATE_USER_PATH,   control.UpdateUser  ).Methods(http.MethodPut)
	handler.HandleFunc(configs.DELETE_USER_PATH,   control.DeleteUser  ).Methods(http.MethodDelete)

	//========================================================
	// group handlers
	//========================================================
	handler.HandleFunc(configs.CREATE_GROUP_PATH, control.CreateGroup ).Methods(http.MethodPost)
	handler.HandleFunc(configs.UPDATE_GROUP_PATH, control.UpdateGroup ).Methods(http.MethodPut)
	handler.HandleFunc(configs.DELETE_GROUP_PATH, control.DeleteGroup ).Methods(http.MethodDelete)

	//returns handle
	return
}

