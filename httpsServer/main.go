package main

import (
"luiz_henrique_tavares/httpsServer/database"
"luiz_henrique_tavares/httpsServer/server"
)

func main() {

	// assure server closing at end of execution
	defer server.StopServer()

	// call db client constructor
	database.CreateClient()

	//starting a server
	server.StartServer()

}
