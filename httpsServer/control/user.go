package control

import ( "fmt"
	"encoding/json"
	"io/ioutil"
	"log"
	"luiz_henrique_tavares/httpsServer/database"
	"luiz_henrique_tavares/httpsServer/models"
	"luiz_henrique_tavares/httpsServer/validation"
	"net/http"
)

func RegisterUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Register ma friend!")

	// retrieve body from request
	body := r.Body

	// declare use entity
	var user models.User

	// parsing io.ReadCLoser to slice of bytes []byte
	bytes, _ := ioutil.ReadAll(body)

	// parses json into user struct
	err := json.Unmarshal(bytes, &user)

	// checks if any error occurs in json parsing
	if err != nil {

		log.Printf("[WARN] problem parsing json body, because, %v\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// checks if struct is a valid one
	if err := validation.Validator.Struct(user); err != nil {

		log.Printf("[WARN] invalid user information, because, %v\n", err)
		w.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	err = database.InsertUser(user)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Write([]byte(`{"message": "success"}`))
}

func LoginUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Hello Loger!")

	// retrieve body from request
	body := r.Body

	// declare use entity
	var login models.LoginUser

	// parsing io.ReadCLoser to slice of bytes []byte
	bytes, _ := ioutil.ReadAll(body)

	// parses json into user struct
	err := json.Unmarshal(bytes, &login)

	// checks if any error occurs in json parsing
	if err != nil {

		log.Printf("[WARN] problem parsing json body, because, %v\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// checks if struct is a valid one
	if err := validation.Validator.Struct(login); err != nil {

		log.Printf("[WARN] invalid user information, because, %v\n", err)
		w.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	resp, err := database.SearchUser(login)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Write([]byte(resp.Password))
}

//creating function fo delete a user
func DeleteUser(w http.ResponseWriter, r *http.Request){
	//take 'id' into queryString
	query := r.URL.Query().Get("id")
	//delete user in user collection
	err := database.DeleteUser(query)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//show message success
	_, _ = w.Write([]byte(`{'message':'delete success'}`))
}
// UpdateUser = Alters an existing user.
func UpdateUser(w http.ResponseWriter, req *http.Request) {
	query := req.URL.Query().Get("id")

    body := req.Body

	// declare user entity
	var updatableUser models.UpdatableUser

	// parsing io.ReadCLoser to slice of bytes []byte
	bytes, _ := ioutil.ReadAll(body)

	// parses json into user struct
	err := json.Unmarshal(bytes, &updatableUser)

    // Updates the user in the DB, returning the resulting user.
    user, err := database.UpdateUser(query, updatableUser); if err != nil {
    	log.Printf("[ERROR] Problem updating user. Error: %v", err)
     		return
 	}

    // Places the user in a JSON to be sent in the response.
    resultUser, err := json.Marshal(user); if err != nil {
        log.Printf("[ERROR] Problem parsing user to JSON. Error: %v", err)

        // Error 500
        w.WriteHeader(http.StatusInternalServerError)
        w.Write([]byte(`{"errors" : "` + err.Error() + `"}`))
        return
    }

    // Success Status 200
    w.WriteHeader(http.StatusOK)

    // Writes the logged user in the response body.
    w.Write(resultUser)
}




