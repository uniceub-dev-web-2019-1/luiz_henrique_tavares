package control

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"luiz_henrique_tavares/httpsServer/database"
	"luiz_henrique_tavares/httpsServer/models"
	"luiz_henrique_tavares/httpsServer/validation"
	"net/http"
)

func CreateGroup (w http.ResponseWriter, r *http.Request) {
	fmt.Println("Create your group ma friend!")

	// retrieve body from request
	body := r.Body

	// declare use entity
	var group models.Group

	// parsing io.ReadCLoser to slice of bytes []byte
	bytes, _ := ioutil.ReadAll(body)

	// parses json into group struct
	err := json.Unmarshal(bytes, &group)

	// checks if any error occurs in json parsing
	if err != nil {

		log.Printf("[WARN] problem parsing json body, because, %v\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// checks if struct is a valid one
	if err := validation.Validator.Struct(group); err != nil {

		log.Printf("[WARN] invalid user information, because, %v\n", err)
		w.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	err = database.CreateGroup(group)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Write([]byte(`{"message": "success"}`))
}



//creating function fo delete a group
func DeleteGroup(w http.ResponseWriter, r *http.Request){
	//take 'id' into queryString
	query := r.URL.Query().Get("id")
	//delete group into group collection
	err := database.DeleteGroup(query)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//show message success
	_, _ = w.Write([]byte(`{'message':'deleted successfully'}`))
}
func UpdateGroup(w http.ResponseWriter, req *http.Request) {
	query := req.URL.Query().Get("id")

    body := req.Body

	// declare user entity
	var updatableGroup models.UpdatableGroup

	// parsing io.ReadCLoser to slice of bytes []byte
	bytes, _ := ioutil.ReadAll(body)

	// parses json into user struct
	err := json.Unmarshal(bytes, &updatableGroup)

    // Updates the user in the DB, returning the resulting user.
    group, err := database.UpdateGroup(query, updatableGroup); if err != nil {
    	log.Printf("[ERROR] Problem updating user. Error: %v", err)
     		return
 	}

    // Places the user in a JSON to be sent in the response.
    resultUser, err := json.Marshal(group); if err != nil {
        log.Printf("[ERROR] Problem parsing user to JSON. Error: %v", err)

        // Error 500
        w.WriteHeader(http.StatusInternalServerError)
        w.Write([]byte(`{"errors" : "` + err.Error() + `"}`))
        return
    }

    // Success Status 200
    w.WriteHeader(http.StatusOK)

    // Writes the logged user in the response body.
    w.Write(resultUser)
}