package configs

//=========================
//	Server HTTP
//=========================
const (

	// defines ip and port address for server instance
	SERVER_ADDR = "localhost:8080"

	// host for mongo db
	MONGO_HOST = "mongodb://localhost:27017"
)

//=========================
//	Paths HTTP
//=========================
const (
	
	//User paths
	REGISTER_USER_PATH  = "/registeruser/"
	LOGIN_USER_PATH     = "/loginuser/"
	UPDATE_USER_PATH    = "/updateuser/"
	DELETE_USER_PATH    = "/deleteuser/"

	//Login paths
	CREATE_GROUP_PATH = "/creategroup/"
	UPDATE_GROUP_PATH = "/updategroup/"
	DELETE_GROUP_PATH = "/deletegroup/"
)

//=========================
//	Collections
//=========================
const (

	// Db collections
	COLLECTION_USERS  = "users"
	COLLECTION_GROUPS = "groups"

)