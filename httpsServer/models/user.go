package models


type (

	User struct {
		Id 			   string  		`json:"id"     validate:"required,min=4"`
		Name           string  		`json:"name"   validate:"required"`
		Password       string  	 	`json:"pass"   validate:"required,min=8"`
		Number         string  	 	`json:"number" validate:"required,numeric"`

	}

	UpdatableUser struct {
		Number         string       `json:"number" validate:"numeric"`
		Name		   string		`json:"name"`
	}

	CreateUser struct {
		Name           string       `json:"name"    validate:"required"`
		Number         string       `json:"number"  validate:"numeric"`
		Password       string       `json:"pass"    validate:"required"`
	}

	LoginUser struct {
		Number     	  string        `json:"number" validate:"required,numeric"`
		Password      string        `json:"pass"   validate:"required"`

	}

)
