package models


type (

	Group struct {
		Id 			   string		 `json:"id" 	  validator:"required"`
		Name           string        `json:"name"     validator:"required"`
		Members        []string    	 `json:"members"  validator:"required"`
	}

	UpdatableGroup struct {
		Name           string        `json:"name"`
		Members        []string      `json:"members"`

	}

)