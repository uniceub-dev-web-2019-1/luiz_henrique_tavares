package database

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"luiz_henrique_tavares/httpsServer/configs"
	"time"
)

var Client *mongo.Client

var Db *mongo.Database

func CreateClient() {

	Client, err := mongo.NewClient(options.Client().ApplyURI(configs.MONGO_HOST))

	if err != nil {
		log.Println("[FATAL] could not create client for database")
		panic(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 1 * time.Second)

	err = Client.Connect(ctx)

	defer cancel()

	if err != nil {
		log.Println("[FATAL] could not connect to database")
		panic(err)
	}

	Db = Client.Database("ludyn")

	return
}

// FindUnavailableName returns true if the name exists in the collection
func FindUnavailableName(name string, dbName string) bool {

	// Creates a context for searching
	ctx, cancel := context.WithTimeout(context.Background(), 5 * time.Second)
	defer cancel()

	// Uses FindOne to search exinsting name
	result, err := Db.Collection(dbName).FindOne(ctx, bson.M{"name": name}).DecodeBytes()

	// Checks if any error occurs
	if err != nil {
		return false
	}

	// Returns true if the result is equal to the name
	return result != nil
}



