package database

import (
	"go.mongodb.org/mongo-driver/bson"
	"luiz_henrique_tavares/httpsServer/models"
	"context"
	"log"
	//"net/http"
)


func CreateGroup(u models.Group) (err error) {

	// select groups collection
	c := Db.Collection("groups")

	// insert group into groups collection
	res, err := c.InsertOne(context.TODO(), u)

	// checks if any error occurs create a group
	if err != nil {
		log.Printf("[ERROR] probleming inserting user: %v %v", err, res.InsertedID)
		return
	}

	return
}

func SearchGroup(l models.Group) (u models.Group, err error) {

	// select groups collection
	c := Db.Collection("groups")

	// create filter
	filter := bson.D{{"name", l.Name}}

	// try to find group with name
	err = c.FindOne(context.TODO(), filter).Decode(&u)

	// checks if any error occurs creating a group
	if err != nil {
		log.Printf("[ERROR] probleming searching group: %v %v", err, u)
		return
	}

	log.Printf("[INFO] searching group: %v %v", err, u)

	return
}

func DeleteGroup(id string) (err error){

	//select group collection
	c := Db.Collection("groups")

	//create filter
	filter := bson.D{{"id",id}}

	//delete group in groups collection
	_, err = c.DeleteOne(context.TODO(),filter)
	if err != nil {
		log.Printf("[ERROR] Probleming deleting group: %v", err)
		return
	}

	//message confirmation in log
	log.Printf("[SUCCESS] Group Deleted")
	return
}

func SearchGroupById(id string)(user models.Group, err error){
	// select groups collection
	c := Db.Collection("groups")

	// create filter
	filter := bson.D{{"id", id}}

	log.Print(id)

	//search group by ID in groups collection
	err = c.FindOne(context.TODO(), filter).Decode(&user)

	// checks if any error occurs insert an group
	if err != nil {
		log.Printf("[ERROR] probleming finding group: %v ", err)
		return
	}

	//message confirmation in log
	log.Printf("[SUCCESS] group found: %v", user)

	return
}
func UpdateGroup(id string, edits interface{}) (user models.Group, err error) {
    user_collection := Db.Collection("groups")

    // Searches for a user that matches the given username and makes the necessary changes.
    err = user_collection.FindOneAndUpdate(context.TODO(), 
                                           bson.M{"id" : id},
                                           bson.M{"$set" : edits}).Decode(&user)
    if err != nil {
        log.Printf("[ERROR] Could not update user. Error: %v", err)
        return
    }

    return

}