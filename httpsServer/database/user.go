package database

import (
	"log"
	"time"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"luiz_henrique_tavares/httpsServer/models"
)


func InsertUser(u models.User) (err error) {

	// select users collection
	c := Db.Collection("users")

	// insert user into users collection
	res, err := c.InsertOne(context.TODO(), u)

	// checks if any error occurs insert an user
	if err != nil {
		log.Printf("[ERROR] probleming inserting user: %v %v", err, res.InsertedID)
		return
	}

	return
}

func SearchUser(l models.LoginUser) (u models.User, err error) {

	// select users collection
	c := Db.Collection("users")

	// Context for search
	ctx, cancel := context.WithTimeout(context.Background(), 3 * time.Second)
	defer cancel()

	// create filter
	filter := bson.D{{"number", l.Number},{"pass", l.Password}}

	// try to find user with login
	err = c.FindOne(ctx, filter).Decode(&u)

	// checks if any error occurs searching an user
	if err != nil {
		log.Printf("[ERROR] probleming searching user: %v %v", err, u)
		return
	}

	log.Printf("[INFO] searching user: %v %v", err, u)

	return
}

func DeleteUser(id string) (err error){

	//select users collection
	c := Db.Collection("users")

	//create filter
	filter := bson.D{{"id",id}}

	//delete user into users collection
	_, err = c.DeleteOne(context.TODO(),filter)
	if err != nil {
		log.Printf("[ERROR] Probleming delete user: %v", err)
		return
	}

	//message confirmation in log
	log.Printf("[SUCCESS] User Deleted")
	return
}

func SearchUserById(id string)(user models.User, err error){
	// select users collection
	c := Db.Collection("users")

	// create filter
	filter := bson.D{{"_id", id}}

	log.Print(id)

	//search user by ID in users collection
	err = c.FindOne(context.TODO(), filter).Decode(&user)

	// checks if any error occurs insert an movie
	if err != nil {
		log.Printf("[ERROR] probleming finding user: %v ", err)
		return
	}

	//message confirmation in log
	log.Printf("[SUCCESS] user found: %v", user)

	return
}

func UpdateUser(id string, edits interface{}) (user models.User, err error) {
    user_collection := Db.Collection("users")

    // Searches for a user that matches the given username and makes the necessary changes.
    err = user_collection.FindOneAndUpdate(context.TODO(), 
                                           bson.M{"id" : id},
                                           bson.M{"$set" : edits}).Decode(&user)
    if err != nil {
        log.Printf("[ERROR] Could not update user. Error: %v", err)
        return
    }

    return

}